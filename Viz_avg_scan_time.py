import os
import pydicom.data
import datetime


def add_colon_change_to_time(str):
    if len(str)==6:
        str_new = str[0:2] + ":" + str[2:4] + ":" + str[4:6]
        time=datetime.datetime.strptime(str_new,'%H:%M:%S')
    else:
        str_new= str[0:2]+":"+str[2:4]+":"+str[4:6]+str[6:]
        time = datetime.datetime.strptime(str_new, '%H:%M:%S.%f')
    return time


curr_dir = os.path.dirname(os.path.realpath(__file__))
os.chdir(curr_dir)
f=open('scan__total_times','w')
for file in os.listdir():
    if file.endswith(".dcm"):
        print(file)
        dcm = pydicom.dcmread(file)
        series=str(dcm.SeriesInstanceUID)
        ContentTime=str(dcm.ContentTime)
        ContentTime=add_colon_change_to_time(ContentTime)
        AcquisitionTime=str(dcm.AcquisitionTime)
        AcquisitionTime = add_colon_change_to_time(AcquisitionTime)
        ExposureTime=str(dcm.ExposureTime)
        TotalTime=ContentTime-AcquisitionTime
        print(' series: ', series, ' content time: ', ContentTime, ' acquisition time: ', AcquisitionTime, ' Total time: ', TotalTime, file=f)
f.close()
