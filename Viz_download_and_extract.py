import os
import sys
import urllib.request
import tarfile

url = sys.argv[1]
curr_dir =  os.path.dirname(os.path.realpath(__file__))
os.chdir(curr_dir)
save_name=url[url.rfind('/')+1:]

ret = urllib.request.urlretrieve(url, save_name)
tar = tarfile.open(save_name)
tar.extractall()
tar.close()

# # url=r'https://s3.amazonaws.com/viz_data/DM_TH.tgz'

