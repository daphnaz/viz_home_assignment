import os
import pydicom
import pydicom.data

#Arrange + Get Patient details + Get number of hospitals.

def join_path(current,add1,add2=None):
    new_path = os.path.join(current, add1,add2)
    return new_path

def arrange_and_analyze_dicom():
    curr_dir = os.path.dirname(os.path.realpath(__file__))
    os.chdir(curr_dir)
    patients=[]
    hospitals=set()
    for file in os.listdir():
        if not file.endswith(".dcm"):
                continue
        file_full_path=os.path.join(curr_dir,file)
        dcm = pydicom.dcmread(file)
        p_name=str(dcm.PatientName)
        p_age=str(dcm.PatientAge)
        p_sex=str(dcm.PatientSex)
        study=str(dcm.StudyInstanceUID)
        series=str(dcm.SeriesInstanceUID)
        hospital=str(dcm.InstitutionName)
        hospitals.add(hospital)
        if not os.path.isdir(p_name):
            p_details=('Patient name: ',p_name," Patient age: ",p_age," Patient sex: ", p_sex)
            patients.append(p_details)
            os.mkdir(p_name)
        study_dir = join_path(p_name, study)
        if not os.path.isdir(study_dir):
            os.mkdir(study_dir)
        series_dir = join_path(p_name, study, series)
        if not os.path.isdir(series_dir):
            os.mkdir(series_dir)
        file_final_path = os.path.join(series_dir, file)
        os.replace(file_full_path, file_final_path)
    return patients,hospitals, len(hospitals)


print(arrange_and_analyze_dicom())